package test.logitech.challenge.exception.base

import java.lang.Exception

/**
 * Created by radhakrishnan on 27/3/18.
 *
 */
class NetworkException : Exception()