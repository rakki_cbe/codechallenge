package test.logitech.challenge.exception.list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.product_list_row_item.view.*
import test.logitech.challenge.R
import test.logitech.challenge.exception.list.model.Product


/**
 * Created by radhakrishnan on 27/3/18.
 *
 */
class ArrayAdapter : RecyclerView.Adapter<ArrayAdapter.Holder>() {
    var list: List<Product> = arrayListOf()
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): Holder {
        val itemView = LayoutInflater.from(parent?.context)
                .inflate(R.layout.product_list_row_item, parent, false)

        return Holder(itemView)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: Holder?, position: Int) {
        if (holder != null)
            holder.setData(list.get(position))
    }

    fun setListData(data: List<Product>) {
        this.list = data
        notifyDataSetChanged()
    }


    inner class Holder(val view: View) : RecyclerView.ViewHolder(view) {
        fun setData(item: Product) {
            name.text = item.name

        }

        val name: TextView

        init {
            name = view.name

        }

    }
}