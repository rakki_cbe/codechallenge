package test.logitech.challenge.exception.list.interfaces

/**
 * Created by radhakrishnan on 27/3/18.
 */

internal interface IfProductPresenter {
    fun getData()
}
