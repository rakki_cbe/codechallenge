package test.logitech.challenge.exception.list

import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.content_main.*
import test.logitech.challenge.R
import test.logitech.challenge.exception.base.NONetworkException
import test.logitech.challenge.exception.base.NetworkException
import test.logitech.challenge.exception.base.NotValidResponse
import test.logitech.challenge.exception.base.UnknownException
import test.logitech.challenge.exception.list.interfaces.IfProductPresenter
import test.logitech.challenge.exception.list.interfaces.IfView
import test.logitech.challenge.exception.list.model.Product
import java.lang.ref.WeakReference

const val MESSAGE_COMPLETED = 1
const val MESSAGE_ERROR = 0

class ListActivity : AppCompatActivity(), IfView {


    override fun showError(error: Exception) {
        val msg = CustomHandler.obtainMessage()
        msg.what = MESSAGE_ERROR
        when (error) {
            is NONetworkException -> msg.obj = R.string.NoInterNet
            is NotValidResponse -> msg.obj = R.string.InvalidResponse
            is NetworkException -> msg.obj = R.string.SomethingWrongWithServer
            is UnknownException -> msg.obj = R.string.SomethingWrong
        }
        CustomHandler.sendMessage(msg)

    }

    fun showToast(id: Int) {
        Toast.makeText(this, getString(id), Toast.LENGTH_LONG).show()
    }

    val adapter: ArrayAdapter = ArrayAdapter()
    override fun updateData(data: List<Product>) {
        val msg = CustomHandler.obtainMessage()
        msg.what = MESSAGE_COMPLETED
        msg.obj = data
        CustomHandler.sendMessage(msg)


    }

    override fun hideProgress() {
        progress_parent.visibility = View.GONE

    }

    override fun clearData() {
        adapter.setListData(arrayListOf())
    }


    private companion object CustomHandler : Handler() {
        private var mActivity: WeakReference<ListActivity>? = null
        fun setActivity(activity: ListActivity) {
            mActivity = WeakReference(activity)
        }

        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)
            val activity = mActivity?.get()
            if (activity != null) {
                activity.hideProgress()
                if (msg?.what == MESSAGE_COMPLETED) {
                    if (msg.obj is List<*>) {

                        activity.adapter.setListData(msg.obj as List<Product>)
                    }
                } else if (msg?.what == MESSAGE_ERROR) {
                    activity.showToast(msg.obj as Int)
                }
            }

        }
    }

    private lateinit var presenter: IfProductPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        CustomHandler.setActivity(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        presenter = ProductPresenter(this)
        presenter.getData()
        val mLayoutManager = LinearLayoutManager(this)
        list.layoutManager = mLayoutManager
        list.itemAnimator = DefaultItemAnimator()
        list.adapter = adapter

    }

    override fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        return connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo.isConnected
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        return if (id == R.id.action_refresh) {
            presenter.getData()
            true
        } else super.onOptionsItemSelected(item)

    }

    override fun showProgress() {
        progress_parent.visibility = View.VISIBLE

    }
}
