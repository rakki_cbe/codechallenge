package test.logitech.challenge.exception.list.interfaces

import test.logitech.challenge.exception.base.IfBaseView
import test.logitech.challenge.exception.list.model.Product

/**
 * Created by radhakrishnan on 27/3/18.
 */

interface IfView : IfBaseView {
    fun updateData(data: List<Product>)
    fun clearData()
    fun isNetworkAvailable(): Boolean
}
