package test.logitech.challenge.exception.list.model

import org.json.JSONObject

/**
 * Created by radhakrishnan on 27/3/18.
 *
 */
object ProductHelper {

    fun getProduct(obj: JSONObject): Product {
        return Product(obj.getString("deviceType"), obj.getString("model"),
                obj.getString("name"))


    }
}