package test.logitech.challenge.exception.network

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import test.logitech.challenge.exception.base.NetworkException
import test.logitech.challenge.exception.base.NotValidResponse
import test.logitech.challenge.exception.base.UnknownException
import test.logitech.challenge.exception.list.model.Product
import test.logitech.challenge.exception.list.model.ProductHelper
import java.io.BufferedReader
import java.io.FileNotFoundException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.TimeoutException

/**
 * Created by radhakrishnan on 27/3/18.
 *
 */
object NetWorkCalls {
    private const val KEY_DEVICE = "devices"

    fun getProductItem(): MutableList<Product> {
        val items = mutableListOf<Product>()
        val urlConnection: HttpURLConnection? = null
        try {
            val url = URL("https://s3.amazonaws.com/harmony-recruit/devices.json")
            val urlConnection = url.openConnection() as HttpURLConnection

            val bufferedReader = BufferedReader(InputStreamReader(urlConnection.inputStream))
            val stringBuilder = StringBuilder()
            var line = bufferedReader.readLine()
            while (line != null) {
                stringBuilder.append(line).append("\n")
                line = bufferedReader.readLine()
            }
            bufferedReader.close()
            try {
                val finalVal = stringBuilder.toString()
                if (finalVal.isNotEmpty()) {
                    val root = JSONObject(finalVal)
                    //
                    if (root.has(KEY_DEVICE)) {
                        val arrayItem = JSONArray(root.getString(KEY_DEVICE))

                        for (i in 0 until arrayItem.length()) {
                            items.add(ProductHelper.getProduct(JSONObject(arrayItem[i].toString())))

                        }
                    }

                }
            } catch (e: JSONException) {
                throw NotValidResponse()
            }

        } catch (e: TimeoutException) {
            throw NetworkException()
        } catch (e: FileNotFoundException) {
            throw NetworkException()
        } catch (e: Exception) {
            throw UnknownException()

        } finally {

            urlConnection?.disconnect()
        }

        return items
    }
}