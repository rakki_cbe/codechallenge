package test.logitech.challenge.exception.list

import test.logitech.challenge.exception.base.BasePresenter
import test.logitech.challenge.exception.base.NONetworkException
import test.logitech.challenge.exception.list.interfaces.IfProductPresenter
import test.logitech.challenge.exception.list.interfaces.IfView
import test.logitech.challenge.exception.network.NetWorkCalls


/**
 * Created by radhakrishnan on 27/3/18.
 */

class ProductPresenter(view: IfView) : BasePresenter<IfView>(view), IfProductPresenter {


    override fun getData() {
        view.clearData()
        view.showProgress()
        if (view.isNetworkAvailable()) {
            val mThread = MyThread()
            mThread.start()
        } else {
            view.hideProgress()
            view.showError(NONetworkException())
        }
    }

    inner class MyThread : Thread() {
        override fun run() {
            try {
                val list = NetWorkCalls.getProductItem()
                view.updateData(list)
            } catch (e: Exception) {
                view.showError(e)
            }

        }

    }

}
