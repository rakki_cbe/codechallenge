package test.logitech.challenge.exception.base

/**
 * Created by radhakrishnan on 27/3/18.
 */

abstract class BasePresenter<T : IfBaseView> internal constructor(view: T) {
    var view: T
        internal set

    init {
        this.view = view

    }
}
