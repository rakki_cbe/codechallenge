package test.logitech.challenge.exception.base

/**
 * Created by radhakrishnan on 27/3/18.
 */

interface IfBaseView {
    fun showProgress()
    fun hideProgress()
    fun showError(error: Exception)

}
