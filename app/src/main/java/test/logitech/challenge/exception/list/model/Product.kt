package test.logitech.challenge.exception.list.model


/**
 * Created by radhakrishnan on 27/3/18.
 *
 */
data class Product(val deviceType: String, val model: String, val name: String)